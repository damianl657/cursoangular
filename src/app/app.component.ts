import { Component } from '@angular/core';
import { Productos } from './productos.model';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'cursoAngular';
  btnDisabled = true;
  auxNombre = '';
  person = {
    name: 'Damian',
    age: 31,
    avatar: "https://www.w3schools.com/tags/img_girl.jpg",
    firstName: 'Leal'

  }
  productos: Productos[] = [
    {
      nombre: "camiseta talle S",
      precio: 5000,
      imagen: './assets/images/camiseta.png',
      categoria: ""
    },
    {
      nombre: "camiseta talle M",
      precio: 5000,
      imagen: './assets/images/camiseta.png',
      categoria: ""
    },
    {
      nombre: "camiseta talle X",
      precio: 5000,
      imagen: './assets/images/camiseta.png',
      categoria: ""
    },
    {
      nombre: "camiseta talle 4",
      precio: 5000,
      imagen: './assets/images/camiseta.png',
      categoria: ""
    }
  ]
  nombres: string[] = ['lucas','sebastian','facundo','jesus'];

  activarButton(){
    this.btnDisabled =! this.btnDisabled;
  }
  incrementarEdad(){
    this.person.age++;
  }
  verScroll(event: Event){
    const elemento = event.target as HTMLElement;
    console.log(elemento.scrollTop)
  }
  cambiarNombre(event: Event){
    const elemento = event.target as HTMLInputElement;
    this.person.name = elemento.value;
  }
  agregarNombre(){
    this.nombres.push(this.auxNombre);
    this.auxNombre = '';
  }
  eliminarNombre(posicion: number){
    this.nombres.splice(posicion,1)
  }
}

